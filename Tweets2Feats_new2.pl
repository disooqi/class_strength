################################################################
## Converts tweets to feats.                                  ##
## Usage: cat tweets | perl Tweets2Feats.pl feats.txt > feats ##
## Last modified: 3/3/2014                                    ##
################################################################

open(IN, $ARGV[0]) or die "Please specify the terms features file\n";
binmode IN, ':utf8';
binmode STDOUT, ':utf8';
while(<IN>){
	if(/(\d+)\t(\S+)/){
		$feat{$2} = $1;
	}
}
close(IN);


binmode STDIN, ':utf8';

while(<STDIN>){
#print;
	if(/^(.+)$/){
		$tid = 1;
		$orgtweet = $1;
		$tweet = &norm($orgtweet);
		$class = 888;
		%feats = ();
		while($tweet =~ /(\S+)/g){
			$w = $1;
			if(exists $feat{$w}){
				$feats{$feat{$w}}++;
			}
		}
		print "$class\t";
		for $f (sort {$a <=> $b} keys %feats){
			print "$f:$feats{$f} ";
		}
		print "#$tid\n";
	}
}

sub norm(){
	my $txt = shift;
	$txt =~ s/(http\S*)//g;
	$txt =~ s/RT //g;
	$txt =~ s/\:\-*[\)D]+/ lol /g;
	$txt =~ s/ l+o+l+ / lol/gi;
	$txt = lc $txt;
	$txt =~ s/[^\w\#\ \@\_]/ /g;
	$txt =~ s/\s+/ /g;
	$txt =~ s/\#(\S+)/\#\1 \1/g;
	return $txt;
}
