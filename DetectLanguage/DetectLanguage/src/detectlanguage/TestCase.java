/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package detectlanguage;

import com.cybozu.labs.langdetect.LangDetectException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 *
 * @author kareemdarwish
 */
public class TestCase
{
    public static void main(String[] args) throws UnsupportedEncodingException, IOException, FileNotFoundException, LangDetectException, ParseException
    {
	Options options = new Options();
	options.addOption("p", true, "profile directory");
	options.addOption("i", true, "input file");
	options.addOption("o", true, "output file");
	options.addOption("col", true, "column to process");
	
	CommandLineParser parser = new DefaultParser();
	CommandLine cmd = parser.parse(options, args);
	
	String profileDir = cmd.getOptionValue("p"); // "/Users/kareemdarwish/RESEARCH/LANGID/GOOGLE/profiles";
	String inputFile = cmd.getOptionValue("i"); //  "/Users/kareemdarwish/RESEARCH/ISLAMOPHOBIA/tweets-of-tweeps.uniq.en.xml";
	String outputFile = cmd.getOptionValue("o"); // "/Users/kareemdarwish/RESEARCH/ISLAMOPHOBIA/tweets-of-tweeps.uniq.en.3.xml";
	if (profileDir == null || inputFile == null || outputFile == null)
	{
	    HelpFormatter formatter = new HelpFormatter();
	    formatter.printHelp("language detection", options );
	}
	else
	{
	    int col = 0;
	    if (cmd.hasOption("col"))
		col = Integer.parseInt(cmd.getOptionValue("col"));
	    
	    DetectLanguage dt = new DetectLanguage(profileDir);
	    dt.detectLanguagesForTweets(inputFile, outputFile, col);
	}
    }
}
