/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package detectlanguage;

import com.cybozu.labs.langdetect.Detector;
import com.cybozu.labs.langdetect.DetectorFactory;
import com.cybozu.labs.langdetect.LangDetectException;
import com.cybozu.labs.langdetect.Language;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kareemdarwish
 */
public class DetectLanguage
{

    /**
     * @param args the command line arguments
     */
    

    DetectLanguage(String profileDir) throws LangDetectException
    {
	init(profileDir);
    }
    
    
    
    public void detectLanguagesForTweets(String inputFile, String outputFile, int column) throws FileNotFoundException, UnsupportedEncodingException, IOException, LangDetectException
    {
	BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile), "UTF8"));
	BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile)));

	String line = "";

	while ((line = br.readLine()) != null)
	{
	    String[] parts = line.split("\t");
	    if (parts.length > column)
	    {
		String text = parts[column];
		String lang = "";
		try
		{
		    lang = detect(line);
		}
		catch (LangDetectException ex)
		{
		    Logger.getLogger(DetectLanguage.class.getName()).log(Level.SEVERE, null, ex);
		}
		bw.write(line + "\t" + lang + "\n");
	    }
	    else
	    {
		bw.write(line + "\n");
	    }
	}
	bw.close();
    }
    
    public void init(String profileDirectory) throws LangDetectException {
        DetectorFactory.loadProfile(profileDirectory);
    }
    public String detect(String text) throws LangDetectException {
        Detector detector = DetectorFactory.create();
        detector.append(text);
        return detector.detect();
    }
    public ArrayList<Language> detectLangs(String text) throws LangDetectException {
        Detector detector = DetectorFactory.create();
        detector.append(text);
        return detector.getProbabilities();
    }
    
}
