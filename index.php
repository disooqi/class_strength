<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $tweetLang = $_POST["lang"];

    if($tweetLang == "0"){
        if (isset($_POST["opt1_submit"])){
            $tweetText = trim($_POST["tweettext"]);
            $LDInputfile = exec("mktemp");
            exec("echo \"$tweetText\" > $LDInputfile");

//            function my_exec($cmd, $input=''){
//                $proc=proc_open($cmd, array(0=>array('pipe', 'r'), 1=>array('pipe', 'w'), 2=>array('pipe', 'w')), $pipes);
//                fwrite($pipes[0], $input);fclose($pipes[0]);
//                $stdout=stream_get_contents($pipes[1]);fclose($pipes[1]);
//                $stderr=stream_get_contents($pipes[2]);fclose($pipes[2]);
//                $rtn=proc_close($proc);
//                return array('stdout'=>$stdout,
//                    'stderr'=>$stderr,
//                    'return'=>$rtn
//                );
//            }

            //var_export(my_exec('java -jar DetectLanguage/DetectLanguage/dist/DetectLanguage.jar -col 0 -i en -o ' . $LDOutputfile . ' -p DetectLanguage/profiles/', 'h\\nel\\nlo'));


            $LDOutputfile = exec("mktemp");
            exec("java -jar DetectLanguage/DetectLanguage/dist/DetectLanguage.jar -col 0 -i $LDInputfile -o $LDOutputfile -p DetectLanguage/profiles/", $out,$status);
            //var_export(my_exec("java -jar \"DetectLanguage/DetectLanguage/dist/DetectLanguage.jar\" -col 0 -i $LDInputfile -o $LDOutputfile -p DetectLanguage/profiles/", 'h\\nel\\nlo'));
            //exec("java -cp \"DetectLanguage/DetectLanguage/build/classes/*:lib\" detectlanguage.TestCase -col 0 -i $LDInputfile -o $LDOutputfile -p DetectLanguage/profiles/", $out,$status);

            $LDOutput = exec("cat $LDOutputfile");
            //echo $LDOutput;
            $LDOutput = explode("\t", $LDOutput);
            //echo $LDOutput[1];
            if($LDOutput[1] == "en"){
                $tweetLang = "2";
            }elseif($LDOutput[1] == "ar"){
                $tweetLang = "1";
            }elseif($LDOutput[1] == "de"){
                $tweetLang = "3";
            }elseif($LDOutput[1] == "fr"){
                $tweetLang = "4";
            }


        }elseif(isset($_POST["opt2_submit"])){

        }
    }

    if ($tweetLang == "1") {
        $termsFeatsFile = "arTerms.feats";
        $modelFile = "arModel";
    } elseif ($tweetLang == "2") {
        $termsFeatsFile = "enTerms.feats";
        $modelFile = "enModel";
    } elseif ($tweetLang == "3") {
        $termsFeatsFile = "deTerms.feats";
        $modelFile = "deModel";
    } elseif ($tweetLang == "4") {

        $termsFeatsFile = "frTerms.feats";
        $modelFile = "frModel";
    } elseif ($tweetLang == "5") {
        $termsFeatsFile = "";
    } else {

        // here we run the language detection module
        $termsFeatsFile = "";
    }
}
?>
<?php
if (isset($_POST["opt2_submit"])){
    $tweetFile = basename($_FILES["tweetsfile"]["name"]);
    if (strlen($tweetFile) > 0){

        $option = 2;
        $target_dir = "/tmp/";
        $target_file = $target_dir . basename($_FILES["tweetsfile"]["name"]);


        if ($_FILES["tweetsfile"]["size"] < 500000) {

            // if everything is ok, try to upload file
            if (move_uploaded_file($_FILES["tweetsfile"]["tmp_name"], $target_file)) {
                //$msg = "The file ". basename( $_FILES["tweetsfile"]["name"]). " has been uploaded.";
                if($tweetLang == "0"){
                    $LDInputfile = exec("mktemp");
                    $LDOutputfile = exec("mktemp");

                    exec("head -1  \"$target_file\" > $LDInputfile");
                    exec("java -jar DetectLanguage/DetectLanguage/dist/DetectLanguage.jar -col 0 -i $LDInputfile -o $LDOutputfile -p DetectLanguage/profiles/", $out,$status);

                    $LDOutput = exec("cat $LDOutputfile");
                    //echo $LDOutput;
                    $LDOutput = explode("\t", $LDOutput);
                    //echo $LDOutput[1];
                    if($LDOutput[1] == "en"){
                        $tweetLang = "2";
                    }elseif($LDOutput[1] == "ar"){
                        $tweetLang = "1";
                    }elseif($LDOutput[1] == "de"){
                        $tweetLang = "3";
                    }elseif($LDOutput[1] == "fr"){
                        $tweetLang = "4";
                    }else{
                        $tweetLang = "0";
                    }

                    if ($tweetLang == "1") {
                        $termsFeatsFile = "arTerms.feats";
                        $modelFile = "arModel";
                    } elseif ($tweetLang == "2") {
                        $termsFeatsFile = "enTerms.feats";
                        $modelFile = "enModel";
                    } elseif ($tweetLang == "3") {
                        $termsFeatsFile = "deTerms.feats";
                        $modelFile = "deModel";
                    } elseif ($tweetLang == "4") {
                        $termsFeatsFile = "frTerms.feats";
                        $modelFile = "frModel";
                    } else {
                        // here we run the language detection module
                    }
                }
                if($tweetLang != "0") {
                    $featfile = exec("mktemp");
                    exec("cat \"$target_file\" | perl Tweets2Feats_new2.pl $termsFeatsFile > $featfile", $rslt, $ev);
                    $classification_out_file = exec("mktemp");
                    exec("./svm_multiclass_classify $featfile $modelFile $classification_out_file", $arr2, $rv2);
                    $downloadName = 'res_' . basename($_FILES["tweetsfile"]["name"]);

                    header('Content-Description: File Transfer');
                    header('Content-Type: text/plain');
                    header('Content-Disposition: attachment; filename="' . $downloadName . '"');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate');
                    header('Pragma: public');
                    header('Content-Length: ' . filesize($classification_out_file));
                    readfile($classification_out_file);
                }
                exit;
//                  header("Location:downloadresult.php?file=".$downloadName);
//                  exit;
            } else {
                echo "<script>alert('Sorry, there was an error uploading your file.');</script>";
            }
        }else{
            echo "<script>alert('Sorry, your file is too large. File should be less than 500KB');</script>";
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Tweet strength</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css"
          integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <script>
        function trial() {
            var tt = document.getElementById("tweettext1");
            var opt1_ls = document.getElementById('opt1_lang');

            <?php
            echo "tt.value = \"".$_POST['tweettext']."\";";
            echo "opt1_ls.value = '".$_POST['lang']."';";
            ?>

        }
    </script>

</head>
<body>


<?php
    if (isset($_POST["opt1_submit"]) && $tweetLang != "0"){
        $tweet = trim($_POST["tweettext"]);

        if(strlen($tweet) > 0){
            $option = 1;
            $featfile = exec("mktemp");
            $feat = exec("echo \"$tweet\" | perl Tweets2Feats_new2.pl $termsFeatsFile");

            exec("echo \"$feat\" >> $featfile", $arr, $rv);
            //echo $featfile;
            //echo "<br>";
            $classification_out_file = exec("mktemp");
            //echo $classification_out_file;
            exec("./svm_multiclass_classify $featfile $modelFile $classification_out_file", $arr2, $rv2);

            $strength_values = exec("cat $classification_out_file");
            //echo $strength_values;
            $strength_values = explode(" ", $strength_values);

            $bestClassID = intval($strength_values[0]);
            $bestClassValue = floatval($strength_values[$bestClassID]);
        }
    }
?>


<header>
    <div class="jumbotron">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-lg-8">
                    <h2 class="qatatsheader">QCRI - Arabic Language Technologies</h2>
                    <h3 class="qatatsheader">Class Strength</h3>
                </div>
            </div>
        </div>
    </div>
</header>

<div id="sitebody">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <ol class="breadcrumb">
                    <li><a href="http://qcri.qa/"></a></li>
                    <li><a href="http://qcri.qa/our-research/arabic-language-technologies">Arabic Language
                            Technologies</a>
                    </li>
                    <li><a href="http://alt.qcri.org/">ALT Server</a></li>
                    <li class="active">Class strength</li>
                </ol>
            </div>
        </div>
<!--        <div class="row">-->
<!--            <div class="col-md-10 col-md-offset-1">-->
<!--                <p>Class Strength is a classifer </p>-->
<!--                Labeling data for training a classifier is a very labor-intensive prcess. In this work we try  to avoid this bottle neck buy using the an already-exist developing-->
<!---->
<!--                Using this way to label the data add an extra feature of being language independent.-->
<!---->
<!--                Class Strength is language independent. In this research work we try to minimize the labor intensive labeled data-->
<!--                by using the colaborated already labelled data in the Web.-->
<!---->
<!---->
<!--                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>-->
<!--                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>-->
<!--            </div>-->
<!--        </div>-->
        <div class="row">
            <br />
            <form action="index.php" method="post">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Option 1:</h3>
                                </div>
                                <div class="panel-body">

                                    <label for="tweettext">Tweet Text</label>
                                    <textarea id="tweettext1" name="tweettext" class="form-control" rows="3"  placeholder="Tweet text"></textarea>
                                    <br />

                                    <label for="sel1" class="col-md-2 control-label">Select language:</label>

                                    <div class="col-md-3">
                                        <select class="form-control" id="opt1_lang" name="lang" class="form-control">
                                            <option value="0" selected>Detect Language</option>
                                            <option value="1">عربي (Arabic)</option>
                                            <option value="2">English</option>
                                            <option value="3" >Deutsch (German)</option>
                                            <option value="4" >français (French)</option>
                                            <option value="5" disabled>русский (Russian)</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <button type="submit" name="opt1_submit" class="btn btn-primary">Show me class strengths</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <form action="index.php" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Option 2:</h3>
                                </div>
                                <div class="panel-body">

                                    <label for="tweetsfile">Press browse to select a text file from your PC to upload and then choose the language of the tweets and press submit</label> (<a href="">sample file</a>)
                                    <input type="file" id="tweetsfile" name="tweetsfile" />
                                    <br />
                                    <label for="sel1" class="col-md-2 control-label">Select language:</label>
                                    <div class="col-md-3">
                                        <select class="form-control" id="opt2_lang" name="lang" class="form-control">
                                            <option value="0">Detect Language</option>
                                            <option value="1">عربي (Arabic)</option>
                                            <option value="2" selected>English</option>
                                            <option value="3">Deutsch (German)</option>
                                            <option value="4">français (French)</option>
                                            <option value="5" disabled>русский (Russian)</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <button type="submit" name="opt2_submit" onclick="tellme('Hamdella 3ala elsalama');" class="btn btn-primary">Upload and generate result</button>
                                    </div>
                                    <!--                    <p class="help-block">Example block-level help text here.</p>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Option 3: <span class="label label-success">new</span></h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="exampleInputFile"><a href="classStrength_0.1.zip">Download ClassStrength</a> package and run it offline on you PC</label>
                                    <br />
                                    <p>A readme file with instructions is attached with the compressed file</p>
<!--                                    <a href="offlinepackage.php"> visit here to download offline package </a>-->
                                    <!--                    <p class="help-block">Example block-level help text here.</p>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <br/>
        <br/>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <label for="tweetsfile">What do you think? Please give us your feedback :)</label>

                <div id="disqus_thread"></div>
                <script>
                    /**
                     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
                     */

                     var disqus_config = function () {
                     this.page.url = 'http://alt.qcri.org/class_strength/';  // Replace PAGE_URL with your page's canonical URL variable
                     this.page.identifier = 'class-strength-main'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                     };

                    (function() {  // DON'T EDIT BELOW THIS LINE
                        var d = document, s = d.createElement('script');

                        s.src = '//class-strength.disqus.com/embed.js';

                        s.setAttribute('data-timestamp', +new Date());
                        (d.head || d.body).appendChild(s);
                    })();
                </script>
                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
            </div>
        </div>
        <div class="row">
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Class strength</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-3">Autos & Vehicles</div>
                                <div class="col-md-9">
                                    <div class="progress">
                                        <div class="progress-bar <?php if($bestClassID == 1){echo 'progress-bar-success progress-bar-striped active';} ?> <?php if(floatval($strength_values[1]) < 0){echo 'progress-bar-danger';} ?>" role="progressbar" aria-valuenow="<?php echo floatval($strength_values[1]); ?>" aria-valuemin="0" aria-valuemax="<?php echo $bestClassValue; ?>" style="min-width: 2em;width:<?php echo (floatval($strength_values[1])/$bestClassValue)*100; ?>%;">
                                            <?php
                                            echo round(floatval($strength_values[1])/$bestClassValue, 3)*100;
                                            echo "%";
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">Comedy</div>
                                <div class="col-md-9">
                                    <div class="progress">
                                        <div class="progress-bar <?php if($bestClassID == 2){echo 'progress-bar-success progress-bar-striped active';} ?> <?php if(floatval($strength_values[2]) < 0){echo 'progress-bar-danger';} ?>" role="progressbar" aria-valuenow="<?php echo floatval($strength_values[2]); ?>" aria-valuemin="0" aria-valuemax="<?php echo $bestClassValue; ?>" style="min-width: 2em;width:<?php echo (floatval($strength_values[2])/$bestClassValue)*100; ?>%;">
                                            <?php
                                            echo round(floatval($strength_values[2])/$bestClassValue, 3)*100;
                                            echo "%";
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">Education</div>
                                <div class="col-md-9">
                                    <div class="progress">
                                        <div class="progress-bar <?php if($bestClassID == 3){echo 'progress-bar-success progress-bar-striped active';} ?> <?php if(floatval($strength_values[3]) < 0){echo 'progress-bar-danger';} ?>" role="progressbar" aria-valuenow="<?php echo floatval($strength_values[3]); ?>" aria-valuemin="0" aria-valuemax="<?php echo $bestClassValue; ?>" style="min-width: 2em;width:<?php echo (floatval($strength_values[3])/$bestClassValue)*100; ?>%;">
                                            <?php
                                            echo round(floatval($strength_values[3])/$bestClassValue,3)*100;
                                            echo "%";
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">Entertainment</div>
                                <div class="col-md-9">
                                    <div class="progress">
                                        <div class="progress-bar <?php if($bestClassID == 4){echo 'progress-bar-success progress-bar-striped active';} ?> <?php if(floatval($strength_values[4]) < 0){echo 'progress-bar-danger';} ?>" role="progressbar" aria-valuenow="<?php echo floatval($strength_values[4]); ?>" aria-valuemin="0" aria-valuemax="<?php echo $bestClassValue; ?>" style="min-width: 2em;width:<?php echo (floatval($strength_values[4])/$bestClassValue)*100; ?>%;">
                                            <?php
                                            echo round(floatval($strength_values[4])/$bestClassValue, 3)*100;
                                            echo "%";
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div><div class="row">
                                <div class="col-md-3">Film & Animation</div>
                                <div class="col-md-9">
                                    <div class="progress">
                                        <div class="progress-bar <?php if($bestClassID == 5){echo 'progress-bar-success progress-bar-striped active';} ?> <?php if(floatval($strength_values[5]) < 0){echo 'progress-bar-danger';} ?>" role="progressbar" aria-valuenow="<?php echo floatval($strength_values[5]); ?>" aria-valuemin="0" aria-valuemax="<?php echo $bestClassValue; ?>" style="min-width: 2em;width:<?php echo (floatval($strength_values[5])/$bestClassValue)*100; ?>%;">
                                            <?php
                                            echo round(floatval($strength_values[5])/$bestClassValue, 3)*100;
                                            echo "%";
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">Gaming</div>
                                <div class="col-md-9">
                                    <div class="progress">
                                        <div class="progress-bar <?php if($bestClassID == 6){echo 'progress-bar-success progress-bar-striped active';} ?> <?php if(floatval($strength_values[6]) < 0){echo 'progress-bar-danger';} ?>" role="progressbar" aria-valuenow="<?php echo floatval($strength_values[6]); ?>" aria-valuemin="0" aria-valuemax="<?php echo $bestClassValue; ?>" style="min-width: 2em;width:<?php echo (floatval($strength_values[6])/$bestClassValue)*100; ?>%;">
                                            <?php
                                            echo round(floatval($strength_values[6])/$bestClassValue, 3)*100;
                                            echo "%";
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div><div class="row">
                                <div class="col-md-3">Howto & Style</div>
                                <div class="col-md-9">
                                    <div class="progress">
                                        <div class="progress-bar <?php if($bestClassID == 7){echo 'progress-bar-success progress-bar-striped active';} ?> <?php if(floatval($strength_values[7]) < 0){echo 'progress-bar-danger';} ?>" role="progressbar" aria-valuenow="<?php echo floatval($strength_values[7]); ?>" aria-valuemin="0" aria-valuemax="<?php echo $bestClassValue; ?>" style="min-width: 2em;width:<?php echo (floatval($strength_values[7])/$bestClassValue)*100; ?>%;">
                                            <?php
                                            echo round(floatval($strength_values[7])/$bestClassValue, 3)*100;
                                            echo "%";
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">Music</div>
                                <div class="col-md-9">
                                    <div class="progress">
                                        <div class="progress-bar <?php if($bestClassID == 8){echo 'progress-bar-success progress-bar-striped active';} ?> <?php if(floatval($strength_values[8]) < 0){echo 'progress-bar-danger';} ?>" role="progressbar" aria-valuenow="<?php echo floatval($strength_values[8]); ?>" aria-valuemin="0" aria-valuemax="<?php echo $bestClassValue; ?>" style="min-width: 2em;width:<?php echo (floatval($strength_values[8])/$bestClassValue)*100; ?>%;">
                                            <?php
                                            echo round(floatval($strength_values[8])/$bestClassValue, 3)*100;
                                            echo "%";
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">News & Politics</div>
                                <div class="col-md-9">
                                    <div class="progress">
                                        <div class="progress-bar <?php if($bestClassID == 9){echo 'progress-bar-success progress-bar-striped active';} ?> <?php if(floatval($strength_values[9]) < 0){echo 'progress-bar-danger';} ?>" role="progressbar" aria-valuenow="<?php echo floatval($strength_values[9]); ?>" aria-valuemin="0" aria-valuemax="<?php echo $bestClassValue; ?>" style="min-width: 2em;width:<?php echo (floatval($strength_values[9])/$bestClassValue)*100; ?>%;">
                                            <?php
                                            echo round(floatval($strength_values[9])/$bestClassValue, 3)*100;
                                            echo "%";
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">Nonprofits & Activism</div>
                                <div class="col-md-9">
                                    <div class="progress">
                                        <div class="progress-bar <?php if($bestClassID == 10){echo 'progress-bar-success progress-bar-striped active';} ?> <?php if(floatval($strength_values[10]) < 0){echo 'progress-bar-danger';} ?>" role="progressbar" aria-valuenow="<?php echo floatval($strength_values[10]); ?>" aria-valuemin="0" aria-valuemax="<?php echo $bestClassValue; ?>" style="min-width: 2em;width:<?php echo (floatval($strength_values[10])/$bestClassValue)*100; ?>%;">
                                            <?php
                                            echo round(floatval($strength_values[10])/$bestClassValue, 3)*100;
                                            echo "%";
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">Pets & Animals</div>
                                <div class="col-md-9">
                                    <div class="progress">
                                        <div class="progress-bar <?php if($bestClassID == 11){echo 'progress-bar-success progress-bar-striped active';} ?> <?php if(floatval($strength_values[11]) < 0){echo 'progress-bar-danger';} ?>" role="progressbar" aria-valuenow="<?php echo floatval($strength_values[11]); ?>" aria-valuemin="0" aria-valuemax="<?php echo $bestClassValue; ?>" style="min-width: 2em;width:<?php echo (floatval($strength_values[11])/$bestClassValue)*100; ?>%;">
                                            <?php
                                            echo round(floatval($strength_values[11])/$bestClassValue, 3)*100;
                                            echo "%";
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">Science & Technology</div>
                                <div class="col-md-9">
                                    <div class="progress">
                                        <div class="progress-bar <?php if($bestClassID == 12){echo 'progress-bar-success progress-bar-striped active';} ?> <?php if(floatval($strength_values[12]) < 0){echo 'progress-bar-danger';} ?>" role="progressbar" aria-valuenow="<?php echo floatval($strength_values[12]); ?>" aria-valuemin="0" aria-valuemax="<?php echo $bestClassValue; ?>" style="min-width: 2em;width:<?php echo (floatval($strength_values[12])/$bestClassValue)*100; ?>%;">
                                            <?php
                                            echo round(floatval($strength_values[12])/$bestClassValue,3)*100;
                                            echo "%";
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">Sports</div>
                                <div class="col-md-9">
                                    <div class="progress">
                                        <div class="progress-bar <?php if($bestClassID == 13){echo 'progress-bar-success progress-bar-striped active';} ?> <?php if(floatval($strength_values[13]) < 0){echo 'progress-bar-danger';} ?>" role="progressbar" aria-valuenow="<?php echo floatval($strength_values[13]); ?>" aria-valuemin="0" aria-valuemax="<?php echo $bestClassValue; ?>" style="min-width: 2em;width:<?php echo (floatval($strength_values[13])/$bestClassValue)*100; ?>%;">
                                            <?php
                                            echo round(floatval($strength_values[13])/$bestClassValue, 3)*100;
                                            echo "%";
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">Travel & Events</div>
                                <div class="col-md-9">
                                    <div class="progress">
                                        <div class="progress-bar <?php if($bestClassID == 14){echo 'progress-bar-success progress-bar-striped active';} ?> <?php if(floatval($strength_values[14]) < 0){echo 'progress-bar-danger';} ?>" role="progressbar" aria-valuenow="<?php echo floatval($strength_values[14]); ?>" aria-valuemin="0" aria-valuemax="<?php echo $bestClassValue; ?>" style="min-width: 2em;width:<?php echo (floatval($strength_values[14])/$bestClassValue)*100; ?>%;">
                                            <?php
                                            echo round(floatval($strength_values[14])/$bestClassValue, 2)*100;
                                            echo "%";
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<!--                            <button type="button" class="btn btn-primary">Save changes</button>-->
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
        </div>
        <br /><br /><br />
        <div class="row">
        </div>

    </div>
</div>
<footer id="sitefooter">
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <span style="font-size: 14px;color: #999999;">Copyright Qatar Computing Research Institute. All rights reserved.</span>
            </div>
        </div>
    </div>
</footer>

<!-- Latest compiled and minified JavaScript -->


<script src="http://code.jquery.com/jquery-2.2.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
        integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
        crossorigin="anonymous"></script>
<script type="text/javascript">
    <?php if (isset($_POST["opt1_submit"]) && $tweetLang != "0"){ ?>
    trial();
    $(window).load(function(){$('#myModal').modal('show');});
    <?php }elseif($tweetLang == "0") {
        echo "alert('We detect \"$LDOutput[1]\". It is not currently supported, Please choose a language from the list if you are sure');";
    }
    ?>
</script>

</body>
</html>
